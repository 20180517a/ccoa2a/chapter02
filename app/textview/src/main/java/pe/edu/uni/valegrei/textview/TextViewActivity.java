package pe.edu.uni.valegrei.textview;

import android.graphics.Color;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class TextViewActivity extends AppCompatActivity {
    TextView textView1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_view);

        textView1 = findViewById(R.id.text_view_1);

        textView1.setOnClickListener(view -> {
            textView1.setText(R.string.text_view_1_en);
            textView1.setBackgroundColor(Color.RED);
            textView1.setTextColor(Color.YELLOW);
        });
    }
}