package pe.edu.uni.cristhianwiki.spinner;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

public class SpinnerActivity extends AppCompatActivity {

    ImageView logo;
    Spinner spinnerLogo;

    ArrayAdapter<CharSequence> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner);

        logo = findViewById(R.id.image_view_logo);
        spinnerLogo = findViewById(R.id.spinner);

        adapter = ArrayAdapter.createFromResource(this, R.array.logos, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerLogo.setAdapter(adapter);

        spinnerLogo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i == 0){
                    logo.setImageResource(R.drawable.a);
                    return;
                }
                if(i == 1){
                    logo.setImageResource(R.drawable.b);
                    return;
                }

                if(i == 2){
                    logo.setImageResource(R.drawable.c);
                    return;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
}