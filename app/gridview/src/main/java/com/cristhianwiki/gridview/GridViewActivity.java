package com.cristhianwiki.gridview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class GridViewActivity extends AppCompatActivity {
    GridView gridView;
    List<String> names = new ArrayList<>();
    List<Integer> images = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_view);

        gridView = findViewById(R.id.grid_view);
        fillArray();
        GridAdapter adapter = new GridAdapter(this, names, images);
        gridView.setAdapter(adapter);

        gridView.setOnItemClickListener((parent, view, position, id) ->
                Toast.makeText(this, names.get(position), Toast.LENGTH_SHORT).show());
    }

    private void fillArray(){
        names.add("Dog");
        names.add("Panda");
        names.add("Cat");

        images.add(R.drawable.doge);
        images.add(R.drawable.panda);
        images.add(R.drawable.cat);
    }
}
