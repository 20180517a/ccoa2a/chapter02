package com.edu.uni.cristhianwiki.game;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GameActivity extends AppCompatActivity {

    TextView textViewLastAttempt, textViewRemainAttempts, textViewHint;
    EditText editTextGuessNumber;
    Button buttonFindGuessNumber;

    Boolean twoDigits, threeDigits, fourDigits;

    Random r = new Random();
    int random;

    int remainAttempts = 10;

    List<Integer> guessesList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        textViewLastAttempt = findViewById(R.id.textview_last_attempt);
        textViewRemainAttempts = findViewById(R.id.textview_remain_attempts);
        textViewHint = findViewById(R.id.textview_hint);
        editTextGuessNumber = findViewById(R.id.edittext_guess_number);
        buttonFindGuessNumber = findViewById(R.id.button_find_guess_number);

        twoDigits = getIntent().getBooleanExtra("TWO", false);
        threeDigits = getIntent().getBooleanExtra("THREE", false);
        fourDigits = getIntent().getBooleanExtra("FOUR", false);

        if (twoDigits) {
            random = 10 + r.nextInt(90);
        }
        if (threeDigits) {
            random = 100 + r.nextInt(900);
        }
        if (fourDigits) {
            random = 1000 + r.nextInt(9000);
        }

        buttonFindGuessNumber.setOnClickListener(v -> {
            String guess = editTextGuessNumber.getText().toString().trim();
            if (guess.equals("")) {
                Toast.makeText(GameActivity.this, R.string.validar_msg_toast, Toast.LENGTH_SHORT).show();
                return;
            }

            remainAttempts--;

            int iGuess = Integer.parseInt(guess);
            guessesList.add(iGuess);

            textViewLastAttempt.setVisibility(View.VISIBLE);
            textViewRemainAttempts.setVisibility(View.VISIBLE);
            textViewHint.setVisibility(View.VISIBLE);

            Resources res = getResources();
            textViewLastAttempt.setText(res.getString(R.string.textview_last_attempt, guess));
            textViewRemainAttempts.setText(res.getString(R.string.textview_remain_attempts, remainAttempts));

            // win
            if (random == iGuess) {
                AlertDialog.Builder builder = new AlertDialog.Builder(GameActivity.this);
                builder.setTitle(R.string.dialog_title);
                builder.setCancelable(false);
                builder.setMessage(res.getString(R.string.game_msg_win, random, (10 - remainAttempts), guessesList));
                builder.setPositiveButton(R.string.yes, (dialog, which) -> {
                    //Inicia nuevo juego
                    Intent intent = new Intent(GameActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                });
                builder.setNegativeButton(R.string.no, (dialog, which) -> {
                    //Salir del aplicativo
                    moveTaskToBack(true);
                    android.os.Process.killProcess(android.os.Process.myPid());
                    System.exit(1);
                }).create().show();
            }
            if (random < iGuess) {
                textViewHint.setText(R.string.textview_decrease);
            }
            if (random > iGuess) {
                textViewHint.setText(R.string.textview_increase);
            }
            // lose
            if (remainAttempts == 0) {
                AlertDialog.Builder builder = new AlertDialog.Builder(GameActivity.this);
                builder.setTitle(R.string.dialog_title);
                builder.setCancelable(false);
                builder.setMessage(res.getString(R.string.game_msg_lose, random, guessesList));
                builder.setPositiveButton(R.string.yes, (dialog, which) -> {
                    //Inicia nuevo juego
                    Intent intent = new Intent(GameActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                });
                builder.setNegativeButton(R.string.no, (dialog, which) -> {
                    //Salir del aplicativo
                    moveTaskToBack(true);
                    android.os.Process.killProcess(android.os.Process.myPid());
                    System.exit(1);
                }).create().show();
            }

            editTextGuessNumber.setText("");
        });
    }
}
